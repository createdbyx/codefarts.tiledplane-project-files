namespace Codefarts.TiledPlane.Scripts
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    using Codefarts.MeshGeneration;

    using UnityEngine;

    /// <summary>
    /// Provides a behaviour for building a tiled plane.
    /// </summary>
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer)), ExecuteInEditMode]
    public class TiledPlane : MonoBehaviour, INotifyPropertyChanged
    {
        /// <summary>
        /// Provides a unity event for when a property has changed.
        /// </summary>
        public UnityEngine.Events.UnityEvent<string> PropertyWasChanged;

        /// <summary>
        /// The property argument pool that holds cached property changed arguments.
        /// </summary>
        private Dictionary<string, PropertyChangedEventArgs> propertyArgumentPool;

        /// <summary>
        /// The allow mesh rebuild flag used to indicate weather or not mesh rebuilding is allowed to occur.
        /// </summary>
        private bool allowMeshRebuild = true;

        /// <summary>
        /// The allow mesh rebuild flag used to indicate weather or not mesh rebuilding is allowed to occur.
        /// </summary>
        public bool AllowMeshRebuild
        {
            get
            {
                return this.allowMeshRebuild;
            }

            set
            {
                var changed = this.allowMeshRebuild != value;
                this.allowMeshRebuild = value;
                if (changed)
                {
                    this.OnPropertyChanged("AllowMeshRebuild");
                }
            }
        }

        /// <summary>
        /// The cached instance of the mesh filter.
        /// </summary>
        private MeshFilter filter;

        /// <summary>
        /// The width value used when the plane is generated.
        /// </summary>
        [SerializeField]
        private float width = 1;

        /// <summary>
        /// The height value used when the plane is generated.
        /// </summary>
        [SerializeField]
        private float height = 1;

        /// <summary>
        /// The number of columns the generated plane will have.
        /// </summary>
        [SerializeField]
        private int columns = 1;

        /// <summary>
        /// The number of rows the generated plane will have.
        /// </summary>
        [SerializeField]
        private int rows = 1;

        /// <summary>
        /// Holds a flag indicating weather opr not hte plane mesh needs to be rebuilt.
        /// </summary>
        private bool needsRebuild;

        /// <summary>
        /// The offset u value that will be applied to each generated triangle texture coordinate.
        /// </summary>
        [SerializeField]
        private float offsetU;

        /// <summary>
        /// The offset v value that will be applied to each generated triangle texture coordinate.
        /// </summary>
        [SerializeField]
        private float offsetV;

        /// <summary>
        /// The scale u value that will be applied to each generated triangle texture coordinate.
        /// </summary>
        [SerializeField]
        private float scaleU = 1;

        /// <summary>
        /// The scale v value that will be applied to each generated triangle texture coordinate.
        /// </summary>
        [SerializeField]
        private float scaleV = 1;

        /// <summary>
        /// Gets or sets the width of the generated plane.
        /// </summary>
        public virtual float Width
        {
            get
            {
                return this.width;
            }

            set
            {
                var changed = this.width != value;
                this.needsRebuild = changed || this.needsRebuild;
                this.width = value;
                if (changed)
                {
                    this.OnPropertyChanged("Width");
                }
            }
        }

        /// <summary>
        /// Gets or sets the height of the generated plane.
        /// </summary>
        public virtual float Height
        {
            get
            {
                return this.height;
            }

            set
            {
                var changed = this.height != value;
                this.needsRebuild = changed || this.needsRebuild;
                this.height = value;
                if (changed)
                {
                    this.OnPropertyChanged("Height");
                }
            }
        }

        /// <summary>
        /// Gets or sets the offset u that will be applied to each triangle across the plane.
        /// </summary>
        public virtual float OffsetU
        {
            get
            {
                return this.offsetU;
            }

            set
            {
                var changed = this.offsetU != value;
                this.needsRebuild = changed || this.needsRebuild;
                this.offsetU = value;
                if (changed)
                {
                    this.OnPropertyChanged("OffsetU");
                }
            }
        }

        /// <summary>
        /// Gets or sets the offset v that will be applied to each triangle across the plane.
        /// </summary>
        public virtual float OffsetV
        {
            get
            {
                return this.offsetV;
            }

            set
            {
                var changed = this.offsetV != value;
                this.needsRebuild = changed || this.needsRebuild;
                this.offsetV = value;
                if (changed)
                {
                    this.OnPropertyChanged("OffsetV");
                }
            }
        }

        /// <summary>
        /// Gets or sets the scale u that will be applied to each triangle across the plane.
        /// </summary>
        public virtual float ScaleU
        {
            get
            {
                return this.scaleU;
            }

            set
            {
                var changed = this.scaleU != value;
                this.needsRebuild = changed || this.needsRebuild;
                this.scaleU = value;
                if (changed)
                {
                    this.OnPropertyChanged("ScaleU");
                }
            }
        }

        /// <summary>
        /// Gets or sets the scale v that will be applied to each triangle across the plane.
        /// </summary>
        public virtual float ScaleV
        {
            get
            {
                return this.scaleV;
            }

            set
            {
                var changed = this.scaleV != value;
                this.needsRebuild = changed || this.needsRebuild;
                this.scaleV = value;
                if (changed)
                {
                    this.OnPropertyChanged("ScaleV");
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of columns that make up the generated plane.
        /// </summary>
        public virtual int Columns
        {
            get
            {
                return this.columns;
            }

            set
            {
                // cap value to prevent 65000 vertex limit and prevent exceptions
                value = Mathf.Clamp(value, 1, 254 - this.rows);
                value = Math.Max(1, value);
                var changed = this.columns != value;
                this.needsRebuild = changed || this.needsRebuild;
                this.columns = value;
                if (changed)
                {
                    this.OnPropertyChanged("Columns");
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of rows that make up the generated plane.
        /// </summary>
        public virtual int Rows
        {
            get
            {
                return this.rows;
            }

            set
            {
                // cap value to prevent 65000 vertex limit and prevent exceptions
                value = Mathf.Clamp(value, 1, 254 - this.columns);
                value = Math.Max(1, value);
                var changed = this.rows != value;
                this.needsRebuild = changed || this.needsRebuild;
                this.rows = value;
                if (changed)
                {
                    this.OnPropertyChanged("Rows");
                }
            }
        }

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        public virtual void Awake()
        {
            this.filter = this.GetComponent<MeshFilter>();
            this.BuildMesh(false);
        }

        /// <summary>
        /// Used to rebuild the plane mesh.
        /// </summary>
        private void BuildMesh(bool forceRebuild)
        {
            if (this.filter == null || (!forceRebuild && !this.allowMeshRebuild))
            {
                return;
            }

            this.filter.sharedMesh = null;
            var mesh = Generate.Plane(this.width, this.height, this.columns, this.rows, this.offsetU, this.offsetV, this.scaleU, this.scaleV);
            mesh.RecalculateBounds();
            mesh.Optimize();
            this.filter.sharedMesh = mesh;
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        public void Update()
        {
            if (this.needsRebuild)
            {
                this.BuildMesh(false);
                this.needsRebuild = false;
            }
        }

        /// <summary>
        /// Occurs when a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises the <see cref="PropertyChanged"/> event.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                // retrieve or store a new property argument class
                PropertyChangedEventArgs args;
                if (!this.propertyArgumentPool.TryGetValue(propertyName, out args))
                {
                    args = new PropertyChangedEventArgs(propertyName);
                    this.propertyArgumentPool.Add(propertyName, args);
                }

                handler(this, args);
            }

            var propertyWasChanged = this.PropertyWasChanged;
            if (propertyWasChanged != null)
            {
                propertyWasChanged.Invoke(propertyName);
            }
        }
    }
}