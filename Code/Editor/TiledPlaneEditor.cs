﻿namespace Codefarts.MeshGeneration.Editor
{
    using Codefarts.GeneralTools.Editor.Controls;
    using Codefarts.Localization;
    using Codefarts.TiledPlane.Scripts;

    using UnityEditor;

    using UnityEngine;

    [CustomEditor(typeof(TiledPlane))]
    public class TiledPlaneEditor : Editor
    {
        private bool showDimentions;

        private TileSelectionControl tileControl;

        private GUIContent showDimentionsTexture;

        private Renderer renderer;

        private int orientationIndex;

        private string[] orientationNames;

        private Vector3 mouseHitPosition;

        /// <summary>
        /// Initializes a new instance of the <see cref="TiledPlaneEditor"/> class.
        /// </summary>
        public TiledPlaneEditor()
        {
            this.showDimentionsTexture = new GUIContent(Resources.Load<Texture2D>("Codefarts.Unity/TiledPlane/Textures/Settings"), "ShowDimentions");
            this.tileControl = new TileSelectionControl();
            this.LoadLocalizeStrings();
        }

        /// <summary>
        /// Used to localize the orientation names and other strings.
        /// </summary>
        private void LoadLocalizeStrings()
        {
            // set localized strings for various controls
            var local = LocalizationManager.Instance;
            this.orientationNames = new[] { local.Get("None"), local.Get("FlipVertical"), local.Get("FlipHorizontal"), local.Get("FlipBoth") };
        }

        /// <summary>
        /// This function is called when the object becomes enabled and active.
        /// </summary>
        public void OnEnable()
        {
            Tools.current = Tool.View;
            Tools.viewTool = ViewTool.FPS;
        }

        /// <summary>
        /// Calculates the position of the mouse over the grid map in local space coordinates.
        /// </summary>
        /// <returns>Returns true if the mouse is over the grid map.</returns>
        private bool UpdateHitPosition()
        {
            // get reference to the grid map component
            var map = this.target as TiledPlane;
            if (map == null)
            {
                return false;
            }

            // build a plane object that 
            // rotate up the same as map rotation
            var directionVector = Vector3.MoveTowards(Vector3.up, map.transform.up, 1);
            //directionVector.Normalize();
            //directionVector *= this.ActiveLayer * map.Depth;

            var p = new Plane(map.transform.up, map.transform.position + directionVector);

            // build a ray type from the current mouse position
            var ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

            // stores the hit location
            var hit = new Vector3();

            // stores the distance to the hit location
            float dist;

            // cast a ray to determine what location it intersects with the plane
            if (p.Raycast(ray, out dist))
            {
                // the ray hits the plane so we calculate the hit location in world space
                hit = ray.origin + (ray.direction.normalized * dist);
            }

            // convert the hit location from world space to local space
            var value = map.transform.InverseTransformPoint(hit);

            var changed = value != this.mouseHitPosition;
            this.mouseHitPosition = value;

            // return true indicating a successful hit test
            return changed;
        }

        /// <summary>
        /// Lets the Editor handle an event in the scene view.
        /// </summary>
        public void OnSceneGUI()
        {
            /*
            if (!this.hasBeenEnabled)
            {
                return;
            }

            // draw scene view controls
            this.DrawInSceneControls();

            // if UpdateHitPosition return true we should update the scene views so that the marker will update in real time
            if (this.UpdateHitPosition())
            {
                Helpers.RepaintSceneView();
            }

            // Calculate the location of the marker based on the location of the mouse
            this.RecalculateHighlightPosition();

            // if a current tool is a available update it
            if (this.currentTool != null)
            {
                this.currentTool.Update();
            }

            // if the mouse is positioned over the layer allow drawing actions to occur
            if (this.IsMouseOverLayer())
            {
                // handle mouse wheel input
                this.HandleMouseWheelInput();
            }

            // draw scene gizmo's
            this.DrawGizmos();
            */
        }

        /// <summary>
        /// Called by unity to draw the inspector GUI.
        /// </summary>
        public override void OnInspectorGUI()
        {
            var builder = this.target as TiledPlane;
            if (builder == null)
            {
                return;
            }

            this.renderer = builder.GetComponent<Renderer>();

            GUILayout.BeginHorizontal();
            this.showDimentions = GUILayout.Toggle(this.showDimentions, this.showDimentionsTexture, GUI.skin.button, GUILayout.Height(32), GUILayout.Width(32));
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            if (this.showDimentions)
            {
                this.DrawDimensions(builder);
            }
            else
            {
                this.DrawTools(builder);
            }

            //var vector2 = EditorGUILayout.Vector2Field("UV Offset", new Vector2(builder.OffsetU, builder.OffsetV));
            //builder.OffsetU = vector2.x;
            //builder.OffsetV = vector2.y;

            //vector2 = EditorGUILayout.Vector2Field("UV Scale", new Vector2(builder.ScaleU, builder.ScaleV));
            //builder.ScaleU = vector2.x;
            //builder.ScaleV = vector2.y;

            //EditorUtility.SetDirty(builder);
        }

        private void DrawTools(TiledPlane builder)
        {
            // get reference to localization manager
            var local = LocalizationManager.Instance;

            GUILayout.BeginVertical();

            this.tileControl.TileWidth = EditorGUILayout.IntField(local.Get("Width"), this.tileControl.TileWidth);
            this.tileControl.TileHeight = EditorGUILayout.IntField(local.Get("Height"), this.tileControl.TileHeight);
            this.tileControl.StartSpacing = EditorGUILayout.Toggle(local.Get("StartsWithSpacing"), this.tileControl.StartSpacing);
            this.tileControl.Spacing = EditorGUILayout.IntField(local.Get("Spacing"), this.tileControl.Spacing);

            // provide a rotation field for specifying a rotation name
            GUILayout.Label(local.Get("Orientation"));
            var index = EditorGUILayout.Popup(this.orientationIndex, this.orientationNames);
            if (index != this.orientationIndex)
            {
                this.orientationIndex = index;
            }

            GUILayout.EndVertical();

            this.tileControl.TextureAsset = this.renderer.sharedMaterials[0].mainTexture;
            this.tileControl.Draw();
        }

        private void DrawDimensions(TiledPlane builder)
        {
            // get reference to localization manager
            var local = LocalizationManager.Instance;

            var isDirty = false;
            var floatValue = EditorGUILayout.FloatField(local.Get("Width"), builder.Width);
            if (floatValue != builder.Width)
            {
                isDirty = true;
                builder.Width = floatValue;
            }

            floatValue = EditorGUILayout.FloatField(local.Get("Height"), builder.Height);
            if (floatValue != builder.Height)
            {
                isDirty = true;
                builder.Height = floatValue;
            }

            var intValue = EditorGUILayout.IntField(local.Get("Columns"), builder.Columns);
            if (intValue != builder.Columns)
            {
                isDirty = true;
                builder.Columns = intValue;
            }

            intValue = EditorGUILayout.IntField(local.Get("Rows"), builder.Rows);
            if (intValue != builder.Rows)
            {
                isDirty = true;
                builder.Rows = intValue;
            }

            //builder.Width = EditorGUILayout.FloatField(local.Get("Width"), builder.Width);
            //builder.Height = EditorGUILayout.FloatField(local.Get("Height"), builder.Height);

            //builder.Columns = EditorGUILayout.IntField(local.Get("Columns"), builder.Columns);
            //builder.Rows = EditorGUILayout.IntField(local.Get("Rows"), builder.Rows);
            if (isDirty)
            {
                EditorUtility.SetDirty(builder);
            }
        }
    }
}